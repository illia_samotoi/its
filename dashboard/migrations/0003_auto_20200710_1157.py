# Generated by Django 3.0.8 on 2020-07-10 09:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0002_message'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='text',
            field=models.TextField(verbose_name='Message'),
        ),
    ]
