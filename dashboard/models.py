from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Issue(models.Model):
    PRIORITY_CHOICES = (
        (0, "Information"),
        (1, "Warning"),
        (2, "Error"),
        (3, "Critical"),
    )

    STATUS_CHOICES = (
        (0, "Pending"),
        (1, "In progress"),
        (2, "Suspended"),
        (3, "Completed")
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    text = models.TextField()
    date_time = models.DateTimeField(auto_now_add=True)
    priority = models.IntegerField(choices=PRIORITY_CHOICES, blank=True, null=True)
    status = models.IntegerField(choices=STATUS_CHOICES)

    def __str__(self):
        return self.title


class Message(models.Model):
    text = models.TextField(verbose_name="Message")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True)
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE)
